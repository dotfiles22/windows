# Windows dotfiles

## Installation

```powershell
  winget install -e --id=Git.Git
  winget install -e --id=Microsoft.PowerShell
  Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
  irm get.scoop.sh | iex
  git clone https://gitlab.com/dotfiles22/windows.git dotfiles
```

Ouvrir votre shell en administrateur dans ce dossier.
```powershell
  cd dotfiles
  Start-Process wt -Verb runAs -ArgumentList "pwsh.exe -NoExit -Command &{Set-Location $PWD}"
```

Lancez le setup
```powershell
  .\Setup.ps1
```
