# Aliases & Custom Envioronment Variables
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Set-Alias -Name su -Value Start-AdminSession
Set-Alias -Name ff -Value Find-File
Set-Alias -Name grep -Value Find-String
Set-Alias -Name touch -Value New-File
Set-Alias -Name df -Value Get-Volume
Set-Alias -Name which -Value Show-Command
Set-Alias -Name ls -Value lsd
Set-Alias -Name ll -Value Get-ChildItemPretty
Set-Alias -Name la -Value Get-ChildItemPretty
Set-Alias -Name l -Value Get-ChildItemPretty
Set-Alias -Name tif Show-ThisIsFine
# Set-Alias -Name nvim -Value Start-Neovide
Set-Alias -Name vim -Value nvim
Set-Alias -Name vi -Value nvim
Set-Alias -Name cat -Value bat

function Find-File {
	<#
		.SYNOPSIS
		Finds a file in the current directory and all subdirectories. Alias: ff
	#>
	
	[CmdletBinding()]
	param (
		[Parameter(ValueFromPipeline, Mandatory = $true, Position = 0)]
		[string]$SearchTerm
	)

	Write-Verbose "Searching for '$SearchTerm' in current directory and subdirectories"
	$result = Get-ChildItem -Recurse -Filter "*$SearchTerm*" -ErrorAction SilentlyContinue

	Write-Verbose "Outputting results to table"
	$result | Format-Table -AutoSize
}


function Get-ChildItemPretty {
    <#
    .SYNOPSIS
        Drop in replacement for Get-ChildItem that lazy loads the Terminal-Icons Module first. This improves profile start up time dramatically.
        Alias: ls, ll, la, l
    #>
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $false, Position = 0)]
        [string]$Path = $PWD
    )
    Write-Verbose "Importing Terminal-Icons module"
    Import-Module Terminal-Icons -ErrorAction SilentlyContinue
    Write-Verbose "Showing children of '$Path'"
    Get-ChildItem $Path | Format-Table -AutoSize
}


# Prompt Setup
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Invoke-Expression (&starship init powershell)
