# Setup script for Windows
# Author: Vincent Gendron

#Requires -RunAsAdministrator

# Symbolik links between destination and dotfiles dir/files
$symlinks = @{
	    "$PROFILE.CurrentUserAllHosts" = ".\Profile.ps1"
#	    "$HOME\AppData\Local\nvim" = ".\nvim"
#	    "$HOME\AppData\Local\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\settings.json" = ".\windowsterminal\settings.json"
#	    "$HOME\.gitconfig" = ".\.gitconfig"
#	    "$HOME\AppData\Roaming\lazygit" = ".\lazygit"
}

# Set working directory
Set-Location $PSScriptRoot
[Environment]::CurrentDirectory = $PSScriptRoot

Write-Host "Installing missing dependencies..."

# Install dependencies - pwsh, git, starship, neovim, choco, zig, ripgrep, fd, sed, lazygit, neovide, bat, nodejs
if (!(Get-Command "pwsh" -ErrorAction SilentlyContinue)) {
	winget install -e --id=Microsoft.PowerShell
}

if (!(Get-Command "git" -ErrorAction SilentlyContinue)) {
	winget install -e --id=Git.Git
}

if (!(Get-Command "starship" -ErrorAction SilentlyContinue)) {
	winget install -e --id Starship.Starship
}

if (!(Get-Command "npm" -ErrorAction SilentlyContinue)) {
    winget install -e --id OpenJS.NodeJS
}

if (!(Get-Command $HOME\AppData\Local\FlowLauncher\Flow.Launcher.exe -ErrorAction SilentlyContinue)) {
	winget install --id=Flow-Launcher.Flow-Launcher -e
}

if (!(Get-Command "choco" -ErrorAction SilentlyContinue)) {
    winget install -e --id=Chocolatey.Chocolatey
}

# Path Refresh
$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")

# Choco Deps
if (!(Get-Command "rg" -ErrorAction SilentlyContinue)) {
	choco install -y ripgrep
}

if (!(Get-Command "fd" -ErrorAction SilentlyContinue)) {
	choco install -y fd
}

if (!(Get-Command "sed" -ErrorAction SilentlyContinue)) {
	choco install -y sed
}

if (!(Get-Command "lazygit" -ErrorAction SilentlyContinue)) {
	choco install -y lazygit
}

if (!(Get-Command "nvim" -ErrorAction SilentlyContinue)) {
	choco install -y neovim
}

if (!(Get-Command "bat" -ErrorAction SilentlyContinue)) {
	choco install -y bat
}

if (!(Get-Command "lsd" -ErrorAction SilentlyContinue)) {
	choco install -y lsd
}

Write-Host "Creating Symbolic Links..."
foreach ($symlink in $symlinks.GetEnumerator()) {
    Get-Item -Path $symlink.Key -ErrorAction SilentlyContinue | Remove-Item -Force -Recurse -ErrorAction SilentlyContinue
    New-Item -ItemType SymbolicLink -Path $symlink.Key -Target (Resolve-Path $symlink.Value) -Force | Out-Null
}
